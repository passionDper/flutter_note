import 'package:flutter/material.dart';
// listView 组件demo
void main () => runApp(MyApp());
class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: 'js pang demo',
      home: Scaffold(
        appBar: new AppBar(
          title: new Text('list view widget'),
        ),
        // body: new Text('list view text'),
        body: new ListView(
          // children 里面可以放一个数组 listView里面就是列表视图
          children: <Widget>[
            // 它接受一个数组，然后有使用了listTile组件（列表瓦片），在组件中放置了图标和文字。
            new ListTile(
              leading: new Icon(Icons.access_time),// 头部图标   通过Icons图标类获取的
              title: new Text('access time'),
            ),
            new ListTile(
              leading: new Icon(Icons.access_alarm),
              title: new Text('access time'),
            ),
            new ListTile(
              leading: new Icon(Icons.backup),
              title: new Text('access time'),
            ),
            // 图片列表开始
            new Image.network(
                'https://dss2.bdstatic.com/8_V1bjqh_Q23odCf/pacific/1910590928.jpg'
            ),
            new Image.network(
                'https://dss2.bdstatic.com/8_V1bjqh_Q23odCf/pacific/1910590928.jpg'
            ),
            new Image.network(
                'https://dss2.bdstatic.com/8_V1bjqh_Q23odCf/pacific/1910590928.jpg'
            ),
            new Image.network(
                'https://dss2.bdstatic.com/8_V1bjqh_Q23odCf/pacific/1910590928.jpg'
            )
          ],
        ),
      ),
    );
  }
}