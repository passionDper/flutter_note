import 'package:flutter/material.dart';
// 主函数入口
void main() =>runApp(MyApp());
// 声明MyApp类
class MyApp extends StatelessWidget{
  // 重写build方法
  @override
  Widget build (BuildContext context) {
    // 返回一个material的风格的组件
    return MaterialApp(
      title: '欢迎来到flutter开发',
      home: Scaffold(
        // 创建一个bar并添加文本
        appBar: AppBar(
          title: Text(
            '欢迎来到FK特产专营平台',
            textAlign: TextAlign.center,// 对齐方式 居中对齐 值可以为 left/center/right/end
            maxLines: 1,// 设置最多显示的行数，比如我们现在只显示1行，类似一个新闻列表的题目
            overflow: TextOverflow.fade,
            style: TextStyle(
              fontSize: 25.0,// 字体大小
              color: Color.fromARGB(255, 255, 150, 150),// 字体颜色
              decoration: TextDecoration.underline,// 设置下划线
              decorationStyle: TextDecorationStyle.solid, // 下划线样式
            ),
          ),
        ),
        body:Center(
          child: Container(
//            child: new Text('hello jspang',style: TextStyle(
//                fontSize: 40.0,
//                color: Color.fromARGB(255, 255, 253, 254) //字体颜色
//            ),),
            // 图片引入的方式 本地图片=> file  项目相对目录图片=> asset 网络图片=>new Image.network()参数为图片地址字符串
            // child: new Image.asset('picture relative url name', width: 100.0,),
            child: new Image.network(
              'https://dss2.bdstatic.com/8_V1bjqh_Q23odCf/pacific/1910590928.jpg',
              width: 400.0, // 宽度
              height: 400.0, // 高度
              color: Colors.lightBlue, //
              colorBlendMode: BlendMode.difference,// 图片的混合色值会影响图片本身的颜色值 本身颜色 + 混合属性的颜色
              // fit: BoxFit.cover , // 缩放模式 和web端的图片的cover类似
              fit: BoxFit.scaleDown, // 图片可以完整显示，但是可能不能填充满。
              // fit: BoxFit.fill, 图片充满元素 一般很容易变形
              repeat: ImageRepeat.noRepeat,// 图片不重复 repeat/repeatX/repeatY
            ),
            alignment: Alignment.topRight, // container的对齐方式 和text元素布局的textAlign不一样 topLeft/topRight每个方向都有属性值对应
            // 设置宽高和颜色、
            width: 500.0,//宽度
            height: 400.0,//高度
            //color: Colors.lightBlue, //设置颜色 这个颜色是背景颜色
            //padding: const EdgeInsets.all(10.0), //内边距为10 这个方法是上下左右的边距都是一样的设置的方法
            padding: const EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 10.0),// LTRB指的就是左上右下的方向的边距
            // margin:EdgeInsets.all(10.0),//设置外边距的方式 左上右下都是一样的大小
            margin: const EdgeInsets.fromLTRB(10.0, 5.0, 2.0, 1.0),//给所有的方向单独设置外边距
            // decoration是 container 的修饰器，主要的功能是设置背景和边框。
            decoration: new BoxDecoration(
                // 设置渐变的类型为线性渐变的方式 （需要注意的是如果你设置了decoration，就不要再设置color属性了，因为这样会冲突）
                gradient: const LinearGradient(
                    colors: [Colors.blue, Colors.greenAccent, Colors.purple] // 颜色开始
                ),
                border:Border.all(width: 2.0, color: Colors.red), //设置边框的属性 宽度和颜色
                borderRadius:BorderRadius.all(new Radius.circular(50.0))
            ),
            //设置边框

          ),
          // child: Text('helloWorld'),
        )
      )
    );
  }
}