import 'package:flutter/material.dart';
void main () => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: 'row横向布局方式',
      home: Scaffold(
        appBar: AppBar(
            title:new Text(
                'row横向布局',
              textAlign: TextAlign.center,
              maxLines: 2,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 25.0,
                // backgroundColor: Colors.green,
                color: Color.fromARGB(255, 255, 150, 150),// 字体颜色
                decoration: TextDecoration.underline,// 设置下划线
                decorationStyle: TextDecorationStyle.solid, // 下划线样式
              ),
            ),

        ),
        body: new Row(// 行布局
          children: <Widget>[
            //解决上面有空隙的问题，可以使用 Expanded来进行解决
            // ，也就是我们说的灵活布局。我们在按钮的外边加入Expanded就可以了
            // 这种布局的方式和web上的flex布局差不多
            Expanded(
              child:  new RaisedButton(
                onPressed: (){ // 按钮按下的事件
                  print('按钮按下');
                },
                color: Colors.redAccent,
                child: new Text('红色按钮'),
              ),
            ),
            Expanded(
              child:  new RaisedButton(
                onPressed: (){ // 按钮按下的事件
                  print('按钮按下');
                },
                color: Colors.yellowAccent,
                child: new Text('黄色按钮'),
              ),
            ),
            Expanded(
              child:  new RaisedButton(
                onPressed: (){ // 按钮按下的事件
                  print('按钮按下');
                },
                color: Colors.greenAccent,
                child: new Text('绿色按钮'),
              ),
            ),
            new RaisedButton(
              onPressed: (){ // 按钮按下的事件
                print('按钮按下');
              },
              color: Colors.yellowAccent,
              child: new Text('普通黄色按钮'),
            ),
//            new RaisedButton(
//              onPressed: (){ // 按钮按下的事件
//                print('按钮按下');
//              },
//              color: Colors.greenAccent,
//              child: new Text('绿色按钮'),
//            )
          ],
        ),
      ),
    );
  }
}