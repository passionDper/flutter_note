import 'package:flutter/material.dart';
void main () => runApp(MyApp());
class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    var stack = new Stack(
      alignment: const FractionalOffset(0.5, 0.5),//它有两个值X轴距离和Y轴距离，值是从0到1的，都是从上层容器的左上角开始算起的
      children: <Widget>[
        new CircleAvatar( // 环形
          backgroundImage: new NetworkImage('http://jspang.com/static//myimg/blogtouxiang.jpg'),
          radius: 100.0,
        ),
        new Container(
          decoration: new BoxDecoration(
            color: Colors.lightGreen,
          ),
          padding: EdgeInsets.all(5.0),
          child: new Text('JSPang 技术胖'),
        )
      ],
    );

    return MaterialApp(
      title: 'stack布局方式',
      home: Scaffold(
        appBar: AppBar(
          title: new Text('stack布局方式'),
          titleSpacing: 2.0,
          centerTitle: true,
        ),
        body: Center(
          child: stack,
        ),
      ),
    );
  }
}