//SnackBar的使用
//SnackBar是用户操作后，显示提示信息的一个控件，类似Toast，会自动隐藏。
// SnackBar是以Scaffold的showSnackBar方法来进行显示的。
//Scaffold.of(context).showSnackBar(SnackBar(content:Text('$result')));


//返回数据的方式
//返回数据其实是特别容易的，只要在返回时带第二个参数就可以了。
// Navigator.pop(context,'xxxx');  //xxx就是返回的参数

import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;

void main(){
  runApp(MaterialApp(
      title:'页面跳转返回数据',
      home:FirstPage()
  ));
}

class FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: AppBar(
        title: new Text('找小姐姐要电话'),
      ),
      body: Center(child: new RouteButton(),),
    );
  }
}

class RouteButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new RaisedButton(
      onPressed: () {
        _navigateToXiaoJieJie(context);
      },
      child: Text('去找小姐姐'),
    );
  }

  _navigateToXiaoJieJie(BuildContext context) async{ // async是异步方法
    Scaffold.of(context).hideCurrentSnackBar(); // 隐藏弹出的toast
    final result = await Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => XiaoJieJie())
    );
    Scaffold.of(context).showSnackBar(SnackBar(content:Text('$result'))); // showSnackBar 和web开发的showToast的效果一样
  }
}

class XiaoJieJie extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
          title:Text('我是小姐姐')
      ),
      body:Center(
          child:Column(
            children: <Widget>[
              RaisedButton(
                child: Text('大长腿小姐姐'),
                onPressed: (){
                  Navigator.pop(context,'大长腿:1511008888');
                },
              ) ,
              RaisedButton(
                child: Text('小蛮腰小姐姐'),
                onPressed: (){
                  Navigator.pop(context,'小蛮腰:1511009999');
                },
              ) ,
            ],
          )
      ) ,
    );
  }
}