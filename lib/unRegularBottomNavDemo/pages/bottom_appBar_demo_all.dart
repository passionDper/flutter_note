import 'package:flutter/material.dart';
import 'each_view.dart';
class BottomAppBarDemoAll extends StatefulWidget {
  _BottomAppBarStateAll createState () => new _BottomAppBarStateAll();
}
class _BottomAppBarStateAll extends State<BottomAppBarDemoAll>{
  List<Widget> _eachView; // 创建视图索引数组
  int _index = 0; // 数组索引，通过改边索引值改边视图
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _eachView = List();// new List()
    _eachView..add(EachView('Home'))..add(EachView('Me')); //添加状态组件
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: _eachView[_index],
      floatingActionButton: FloatingActionButton( //浮动得动作按钮
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) {
                return EachView('New page');
              }
            )
          );
        },
        child: Text('add',textAlign: TextAlign.center,style: TextStyle(
          color: Colors.white,
        ),),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,// 设置浮动得样式 centerDocked：居中
      bottomNavigationBar: BottomAppBar(
        color: Colors.lightBlue,
        shape: CircularNotchedRectangle(),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.home),
              color: Colors.white,
              onPressed: () {
                setState(() {
                  _index = 0;
                });
              },
            ),
            IconButton(
              icon: Icon(Icons.airport_shuttle),
              color: Colors.white,
              onPressed: () {
                setState(() {
                  _index = 1;
                });
              },
            )
          ],
        ),
      ),
    );
  }
}