import 'package:flutter/material.dart';

void main () => runApp(MyApp());
class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: '横向listView列表',
      home: Scaffold(
        body: Center(
          child: Container(
            height: 200.00,
            child: new ListView(
              // istView组件的scrollDirection属性只有两个值，一个是横向滚动，
              // 一个是纵向滚动。默认的就是垂直滚动，所以如果是垂直滚动，我们一般都不进行设置。
              //Axis.horizontal:横向滚动或者叫水平方向滚动。
              //Axis.vertical:纵向滚动或者叫垂直方向滚动。
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                new Container(
                  width: 180.0,
                  color: Colors.lightBlue,
                  child: Center(
                    child: new Text('横向滚动listview'),
                  ),
                ),
                new Container(
                    width: 180.0,
                    color: Colors.lightBlue,
                    child: Center(
                      child: new Text('横向滚动listview'),
                    ),
                ),
                new Container(
                    width: 180.0,
                    color: Colors.lightBlue,
                    child: Center(
                      child: new Text('横向滚动listview'),
                    ),
                ),
                new Container(
                    width: 180.0,
                    color: Colors.lightBlue,
                    child: Center(
                      child: new Text('横向滚动listview'),
                    ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}