/*卡片组件*/
import 'package:flutter/material.dart';
void main () => runApp(MyApp());
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    // 代码中使用了一个垂直布局组件Column组件，然后利用了ListTile实现内部列表，
    // 这里需要说明的是ListTile不光可以使用在ListView组件中，然后容器组件其实都可以使用。代码如下.
    var card = new Card(
      child: Column(
        children: <Widget>[
          ListTile(
            title: new Text(//标题
              "吉林省吉林市昌邑区",
              style: new TextStyle(
                fontWeight: FontWeight.bold,// 加粗字体
              ),
            ),
            subtitle: new Text('技术胖17729839862'),// 底部的标题 类似于副标题
            leading: new Icon( // 前面的图标
              Icons.account_box,
              color:Colors.lightBlue,
            ),
          ),
          ListTile(
            title: new Text(//标题
              "四川省成都市",
              style: new TextStyle(
                fontWeight: FontWeight.bold,// 加粗字体
              ),
            ),
            subtitle: new Text('技术胖17729839862'),// 底部的标题 类似于副标题
            leading: new Icon( // 前面的图标
              Icons.account_box,
              color:Colors.lightBlue,
            ),
          ),
          ListTile(
            title: new Text(//标题
              "四川省达州市",
              style: new TextStyle(
                fontWeight: FontWeight.bold,// 加粗字体
              ),
            ),
            subtitle: new Text('技术胖17729839862'),// 底部的标题 类似于副标题
            leading: new Icon( // 前面的图标
              Icons.account_box,
              color:Colors.lightBlue,
            ),
          )
        ],
      ),
    );
    return MaterialApp(
      title: '卡片组件布局方式',
      home:Scaffold(
        appBar: new AppBar(
          title: new Text('卡片布局列表'),
        ),
        body: Center(
          child: card,
        ),
      )
    );
  }
}