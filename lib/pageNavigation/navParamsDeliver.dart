import 'package:flutter/material.dart';
// Awesome Flutter snippets组件的使用
//因为这节课我们的代码有些多，这时候我们要加快敲代码的速度，可以使用VSCode 中的
// Awesome Flutter snippets插件。它可以帮忙我们快速生成常用的Flutter代码片段。
//比如输入stlss就会给我们生成如下代码：

class Product {
  final String title; //商品标题
  final String description; // 商品描述
  Product(this.title, this.description); //构造方法
}

void main () {
  runApp(MaterialApp(
    title: '数据传递案例',
    home: ProductList(
      products:List.generate(20, (i) => Product('商品 $i', '这是一个商品详情,编号为$i')
    )
  ))
  );
}

class ProductList extends StatelessWidget {
  ProductList({Key key, @required this.products}) :super(key: key); // 调用类 传递参数
  final List<Product> products;

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('商品列表')),
        body: ListView.builder(
          itemCount: products.length,
          itemBuilder: (context, index) {
            return ListTile(
                title: Text(products[index].title),
                onTap: () {},
                subtitle: new Text(products[index].description) ,
                leading: Icon(
                  Icons.access_alarm,
                  color: Colors.greenAccent,
                ),
            );
          },
        )
    );
  }
}
