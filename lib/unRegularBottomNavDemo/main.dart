import 'package:flutter/material.dart';
import 'pages/bottom_appBar_demo_all.dart';
// 自定义主题样本
//Flutter支持自定义主题，如果使用自定义主题，设置的内容项是非常多的，
// 这可能让初学者头疼，Flutter贴心的为给我们准备了主题样本。
//primarySwatch ：现在支持18种主题样本了。
void main () => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: 'Fliutter bottom unregular demo',
      theme: ThemeData(
        primarySwatch: Colors.lightBlue
      ),
      home:BottomAppBarDemoAll(),
    );
  }
}