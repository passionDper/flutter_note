import 'package:flutter/material.dart';

void main () => runApp(MyApp());
class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: 'list view widget',
      home: Scaffold(
        appBar: AppBar(
          title: new Text('组件封装嵌套,解决全部组件嵌套的问题'),
        ),
        body: Center(
          child: Container(
            color: Colors.green,
            height: 200.0,
            padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
            width: 500.0,
            child: MyList(),// 这里调用下面封装的MyList的组件类
          ),
        ),
      ),
    );
  }
}
// 这是封装的一个组件类
class MyList extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView(
      scrollDirection: Axis.horizontal,
      children: <Widget>[
        new Container(
          height: 180.0,
          width: 180.0,
//          color: Colors.lightBlue,
          child: new Text('组件类封装'),
        ),
        new Container(
          width: 180.0,
          color: Colors.lightBlue,
          child: new Text('组件类封装'),
        ),
        new Container(
          width: 180.0,
          color: Colors.lightBlue,
          child: new Text('组件类封装'),
        )
      ],
    );
  }
}