import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
void main () => runApp(MyApp());

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: 'cloumn layout way',
      home: Scaffold(
        appBar: AppBar(
          title: new Text('垂直方向布局'),
        ),
        body:new Column(
          //main轴：如果你用column组件，那垂直就是主轴，如果你用Row组件，那水平就是主轴。
          //cross轴：cross轴我们称为幅轴，。
          // 比如Row组件，那垂直就是幅轴，Column组件的幅轴就是水平是和主轴垂直的方向方向的。
          crossAxisAlignment: CrossAxisAlignment.center, // 副轴对齐方式 center 居中对齐，end 居右对齐
          mainAxisAlignment: MainAxisAlignment.center,// 主轴对齐的方式
          // 相对屏幕居中 Center()包裹起来就会根据屏幕进行居中

          children: <Widget>[
            Text('I am JSPang',
              textAlign: TextAlign.center,
              maxLines: 2,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 15.0,
                color: Color.fromARGB(255, 255, 150, 150),// 字体颜色
                decoration: TextDecoration.underline,// 设置下划线
                decorationStyle: TextDecorationStyle.solid, // 下划线样式
              ),
            ),
            Expanded(
              child: Center(
                child:Text('my website is jspang.com'),
              ),
            ),
            Expanded(
              child: Center(
                child:Text('my website is jspang.com'),
              ),
            ),
            Expanded(
              child: Center(
                child:Text('my website is jspang.com'),
              ),
            ),
            Center(
              child:Text('my website is jspang.com'),
            ),
            Center(
              child:Text('my website is jspang.com'),
            ),
            Center(
              child:Text('my website is jspang.com'),
            ),
            Center(
              child:Text('my website is jspang.com'),
            ),
            Text('my website is jspang.com'),
            Text('I love coding')
          ],
        ),
      ),
    );
  }
}