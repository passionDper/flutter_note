import 'package:flutter/material.dart';
import 'pages/FirstPage.dart';
void main () => runApp(MyApp());
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: '路由动画',
      theme: new ThemeData(
        primarySwatch: Colors.blue
      ),
      home:FirstPage() ,
    );
  }
}