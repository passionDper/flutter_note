import 'package:flutter/material.dart';
void main () => runApp(MyApp());
class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: '欢迎来到GRID VIEW页面',
      home: Scaffold(
        body: GridView.count(
          //padding:表示内边距，这个小伙伴们应该很熟悉。
          //crossAxisSpacing:网格间的空当，相当于每个网格之间的间距。
          //crossAxisCount:网格的列数，相当于一行放置的网格数量。
          padding: const EdgeInsets.all(50.0),// 内边距
          crossAxisCount: 3,// 每行显示几列
          crossAxisSpacing: 10.0,// 每列之间的间距
          children: <Widget>[
            const Text('I AM JSPANG'),
            const Text('I AM JSPANG'),
            const Text('I AM JSPANG'),
            const Text('I AM JSPANG')
          ],
        ),
      ),
    );
  }
}