import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'pages/home_screen.dart';
import 'pages/email_screen.dart';
import 'pages/pages_screen.dart';
import 'pages/airplay_screen.dart';

class BottomNavigationWidget extends StatefulWidget {
  _BottomNavigationWidgetState createState() => new _BottomNavigationWidgetState();
}

class _BottomNavigationWidgetState extends State<BottomNavigationWidget> {
  final _bottomNavigationColor = Colors.blue;
  int _currentIndex = 0;// 当前tabBar的index的值
  List<Widget> list = new List(); // 创建一个list组件实例

  @override
  void initState(){ // initState这个方法可以初始化值 给一个组件list添加组件
    list
      ..add(new HomeScreen())
      ..add(new AirplayScreen())
      ..add(new PagesScreen())
      ..add(new EmailScreen());
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: list[_currentIndex],// body部分
      //底部导航栏开始
      bottomNavigationBar: new BottomNavigationBar( //底部导航栏属性
        type: BottomNavigationBarType.fixed,// 底部导航条类型 fixed
        items: [
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
                color: _bottomNavigationColor,
            ),
            title: Text(
              'Home',
              style: TextStyle(
                color: _bottomNavigationColor
              ),
            )
          ),
          BottomNavigationBarItem(
              icon:Icon(
                Icons.email,
                color:_bottomNavigationColor,
              ),
              title:Text(
                  'Email',
                  style:TextStyle(color:_bottomNavigationColor)
              )
          ),
          BottomNavigationBarItem(
              icon:Icon(
                Icons.pages,
                color:_bottomNavigationColor,
              ),
              title:Text(
                  'Pages',
                  style:TextStyle(color:_bottomNavigationColor)
              )
          ),
          BottomNavigationBarItem(
              icon:Icon(
                Icons.airplay,
                color:_bottomNavigationColor,
              ),
              title:Text(
                  'AipPlay',
                  style:TextStyle(color:_bottomNavigationColor)
              )
          ),
        ],
        currentIndex: _currentIndex,//当前的tab所在的目录
        onTap: (int index) { //点击底部菜单的方法 参数是点击tab项的index下标值
          setState(() {// setState 设置变量值的方法
            _currentIndex = index;
          });
        },
      ),
    );
  }
}