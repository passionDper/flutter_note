import 'package:flutter/material.dart';
//List是Dart的集合类型之一,其实你可以把它简单理解为数组（反正我是这么认为的），
// 其他语言也都有这个类型。它的声明有几种方式：
//var myList = List(): 非固定长度的声明。
//var myList = List(2): 固定长度的声明。
//var myList= List<String>():固定类型的声明方式。
//var myList = [1,2,3]: 对List直接赋值。
void main () => runApp(MyApp(
    items: new List<String>.generate(1000, (i)=> "Item $i")
));

class MyApp extends StatelessWidget{
  final List<String> items;
  MyApp({Key key, @required this.items}):super(key:key);
  // 这是一个构造函数，除了Key，我们增加了一个必传参数，这里的@required意思就必传。
  // :super如果父类没有无名无参数的默认构造函数，则子类必须手动调用一个父类构造函数。
  @override
  Widget build(BuildContext context ){
    return MaterialApp(
      title:'ListView widget',
      home:Scaffold(
          body:new ListView.builder( // 构建动态列表
              itemCount:items.length,// items的长度
              itemBuilder:(context,index){ // itemBuilder
                return new ListTile(
                  title:new Text('${items[index]}'),// '${items[index]}'
                );
              }
          )
      ),
    );
  }
}