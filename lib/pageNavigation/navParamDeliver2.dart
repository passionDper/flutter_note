import 'package:flutter/material.dart';
// 我们还是使用Navigator组件，然后使用路由MaterialPageRoute传递参数，具体代码如下。

//传递的数据结构,也可以理解为对商品数据的抽象
class Product{
  final String title;  //商品标题
  final String description;  //商品描述
  Product(this.title,this.description);
}

void main () {
  runApp(
    MaterialApp(
      title: '数据传递实例',
      home: ProductList(
        products:List.generate(20, (i) => Product('商品$i','这是一个商品详情$i'))
      ),
    )
  );
}

class ProductList extends StatelessWidget{
  final List<Product> products;
  ProductList({Key key,@required this.products}):super(key:key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title:Text('商品列表')),
        body:ListView.builder(
          itemCount:products.length,
          itemBuilder: (context,index){
            return ListTile(
                title:Text(products[index].title),
                subtitle: Text(products[index].description),
                leading: Icon(
                  Icons.add_a_photo,
                  color: Colors.redAccent,
                ),
                onTap:(){
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder:(context)=>new ProductDetail(product:products[index])
                      )
                  );
                }
            );
          },
        )
    );
  }
}

class ProductDetail extends StatelessWidget {
  final Product product;
  ProductDetail({Key key,@required this.product}):super(key:key);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: AppBar(
        title: Text('${product.title}'),
      ),
      body: Center(
        child: Text(
          '${product.description}'
        ),
      ),
    );
  }
}