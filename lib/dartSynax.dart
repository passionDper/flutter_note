dynamic a;
Object b;

main() {
  printLengths();
  finalAndConst();
  print(isNoble(1));
  //报错，isNoble不是bool类型
  test(isNobleOne);
  say("hi world");
  execute(() => print("xxx"));
  print(saySelectParam('Bob', 'Howdy', 'Cgf')); //结果是： Bob says Howdy

  enableFlags(bold: true, hidden: false); // 选入指定参数 bold对象形参的bold hidden参数对应的形参的hidden

  futureDelayedOperation();// 延时方法 future.delayed()

  futureCatchError();// future.catchError抛错的方式

  futureOnError();// Future onError抛错 所有结果都会打印出来

}

/* 一：dynamic和Object
Object 是Dart所有对象的根基类，也就是说所有类型都是Object的子类(包括Function和Null)，
所以任何类型的数据都可以赋值给Object声明的对象. dynamic与var一样都是关键词,
声明的变量可以赋值任意对象。 而dynamic与Object相同之处在于,他们声明的变量可以在后期改变赋值类型。*/
printLengths() {
  // no warning
  a = '123';
  b = "";
  print(a.length); // 赋值数字123的时候 会报错 说的是int数据类型没有length的属性
  // warning:
  // The getter 'length' is not defined for the class 'Object'
  // print(b.length); // 这里就会报错 应该使用Object的属性方法
}

/* *
 * 二：final const关键字
 * 如果您从未打算更改一个变量，那么使用 final 或 const，不是var，也不是一个类型。
 * 一个 final 变量只能被设置一次，两者区别在于：const 变量是一个编译时常量，
 * final变量在第一次使用时被初始化。被final或者const修饰的变量，变量类型可以省略，如
 * */

finalAndConst () {
  //可以省略String这个类型声明
  final str = "hi world";
  //final String str = "hi world";
  const str1 = "hi world";
  //const String str1 = "hi world";
  // str = 'hello world'; // 这里报错 final关键字只能被赋值一次 初始化一次
  // str1 = "hello world"; // 也不能进行二次赋值
  print('final的值:'+ str + ' const的值:' + str1);
}

/* *
 * 三：函数方法 function
 *  定义方法的时候必须使用返回类型声明和参数的数据类型定义
 * */
var _nobleGases = '123';
// 1.函数声明
bool isNoble (int atomicNumber) {
  return _nobleGases[atomicNumber] != null;
}
 isNobleOne () {
   return _nobleGases[1] != null;
 }
// 2. Dart函数声明如果没有显式声明返回值类型时会默认当做dynamic处理，注意，函数返回值没有类型推断：
typedef CALLBACK(); // 定义一个方法 传递回调函数名 形参里面可以拿来使用
void test(CALLBACK cb){
  print(cb());
}

// 3.对于只包含一个表达式的函数，可以使用简写语法 相当于js里面的箭头函数

bool isNobleSimplify (int atomicNumber) => _nobleGases [ atomicNumber ] != null ;

// 4.函数作为变量
var say = (String str) => {
  print(str)
};

// 5.函数作为参数传递
void execute (var callBack) {
  callBack();
}
// 6.可选位置参数 包装一组函数参数，用[]标记为可选的位置参数，并放在参数列表的最后面：// 这里的参数就是可选参数（可传可不传）
String saySelectParam (String from, String msg, [String device] ) {
  var result = '$from says $msg';
  if (device != null) {
    result = '$result with a $device';
  }
  return result;
}
// 7.可选的命名参数  定义函数时，使用{param1, param2, …}，放在参数列表的最后面，用于指定命名参数。

//设置[bold]和[hidden]标志
void enableFlags({bool bold, bool hidden}) { //指定的命名的参数
  // ...
  print('bold: ' + bold.toString() + ' hidden: ' + hidden.toString());

  print('bold: $bold hidden: $hidden');// $变量名/参数名 和js的`${变量名}`的效果是一样的
}

/*
三：异步支持 future
Dart类库有非常多的返回Future或者Stream对象的函数。 这些函数被称为异步函数：
它们只会在设置好一些耗时操作之后返回，比如像 IO操作。而不是等到这个操作完成。
async和await关键词支持了异步编程，允许您写出和同步代码很像的异步代码。
* */

// 1.future.then()
futureDelayedOperation () {
  Future.delayed(new Duration(seconds: 5), (){ // delayed延时操作的方法 new Duration是调用延时的时间值
    return "hi world!";
  }).then((data){
    print(data);
  });
}

// 2.Future.catchError()
futureCatchError () {
  Future.delayed(new Duration(seconds: 2),(){
    //return "hi world!";
    throw AssertionError("Error"); // 抛了这个错之后catchError里面是捕捉到了错误，但是then()里面的代码不会执行
  }).then((data){
    //执行成功会走到这里
    print("success");
  }).catchError((e){
    //执行失败会走到这里
    print(e); // Assertion failed
  });
}

// 3.Future.onError() 这个onError是写在then()方法里面的 抛错了之后所有打印结果都会打印
futureOnError () {
  Future.delayed(new Duration(seconds: 2), () {
    //return "hi world!";
    throw AssertionError("Error");
  }).then((data) {
    print("success");
  }, onError: (e) {
    print(e);
  });
}




